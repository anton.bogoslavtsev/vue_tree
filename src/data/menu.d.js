export const menu = [
    {
        id: 1,
        name: "List 1",
        checked: false,
        indeterminate: false,
        countable: false,
        colorable: false,
        children: [
            {
                id: 11,
                name: "Item 1",
                checked: false,
                indeterminate: false,
                countable: true,
                colorable: true,
                count: 0,
                color: "#ffffff",
                children: [
                    {
                        id: 111,
                        name: "Item 1.1",
                        checked: false
                    }
                ]
            },
            {
                id: 12,
                name: "Item 2",
                checked: false,
                indeterminate: false,
                countable: true,
                colorable: true,
                count: 0,
                color: "#ffffff",
                children: [ ]
            },
            {
                id: 13,
                name: "Item 3",
                checked: false,
                indeterminate: false, 
                countable: true,
                colorable: true,
                count: 0,
                color: "#ffffff",
                children: [ ]
            },
        ],
    }
];