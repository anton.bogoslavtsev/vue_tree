import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: () => ({
    menu: [],
  }),
  mutations: {
    updateMenu(state, arr) {
      state.menu = arr;
    },
    updateMenuItem(state, node) {
      state.menu = {
        ...state.menu,
        [node.id]: node,
      };
    },
    updateSelected(state, arr) {
      state.selectedNodes = arr;
    },
  },
  getters: {
    getMenu: (state) => state.menu,
  },
  actions: {
    setMenu(_, menu) {
      this.commit("updateMenu", menu);
    },
    nodeSelection(_, menuItem) {
      const currentMenu = JSON.parse(JSON.stringify(this.getters.getMenu));
      markNodeSelectionState(menuItem.id, currentMenu, menuItem.checked, null);
      this.commit("updateMenu", currentMenu);
    },
    nodeCountChanged(_, menuItem) {
      const currentMenu = JSON.parse(JSON.stringify(this.getters.getMenu));
      changeNodeCount(menuItem.id, currentMenu, menuItem.count);
      this.commit("updateMenu", currentMenu);
    },
    nodeColorChanged(_, menuItem) {
      const currentMenu = JSON.parse(JSON.stringify(this.getters.getMenu));
      changeNodeColor(menuItem.id, currentMenu, menuItem.color);
      this.commit("updateMenu", currentMenu);
    },
  },
});

function markNodeSelectionState(id, currentMenu, state, parent) {
  currentMenu.forEach((element) => {
    if (element.id === id) {
      element.checked = state;
      if (element.children && element.children.length > 0) {
        markSelectionForAllNodes(element.children, element.checked);
      }
      if (
        parent &&
        parent.children.some((e) => !e.checked) &&
        parent.children.some((e) => e.checked)
      ) {
        parent.indeterminate = true;
      } else if (parent) {
        parent.indeterminate = false;
        parent.checked = state;
      }
    } else if (element.children && element.children.length > 0) {
      markNodeSelectionState(id, element.children, state, element);
    }
  });
}

function markSelectionForAllNodes(currentMenu, selectionState) {
  currentMenu.forEach((element) => {
    element.checked = selectionState;
    if (element.children && element.children.length > 0) {
      markSelectionForAllNodes(element.children, selectionState);
    }
  });
}

function changeNodeCount(id, currentMenu, state) {
  currentMenu.forEach((element) => {
    if (element.id === id) {
      element.count = state;
    } else if (element.children && element.children.length > 0) {
      changeNodeCount(id, element.children, state);
    }
  });
}

function changeNodeColor(id, currentMenu, state) {
  currentMenu.forEach((element) => {
    if (element.id === id) {
      element.color = state;
    } else if (element.children && element.children.length > 0) {
      changeNodeColor(id, element.children, state);
    }
  });
}
