import Vue from 'vue'
import App from './App.vue'
import { store } from './store/store'
import * as menu_data from "./data/menu.d.js";

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store,
}).$mount('#app')

store.dispatch('setMenu', menu_data.menu);