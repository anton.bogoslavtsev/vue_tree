# vue-tree
Тестовое задание
Использовал Vue,Vuex

Требования:
При клике на чекбокс уровня List должны выбираться все вложенные items. Если все вложенные items уже выбраны, то снимать с них выделение. Если выбран хотя бы один item, но не все, то отображать точку в чекбоксе родительского List'a.
Должна быть возможность изменять параметр "количество" у items (обычного <input> достаточно, значение должно быть >= 0);
Должна быть возможность изменять параметр "цвет" у items (<input type="color">);
Все действия на странице должны происходить без ее перезагрузки;
Все изменяемые значения должны быть реактивными;
У каждого item изначально должны быть указаны параметры "количество"" и "цвет" по умолчанию (задать произвольно).
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
